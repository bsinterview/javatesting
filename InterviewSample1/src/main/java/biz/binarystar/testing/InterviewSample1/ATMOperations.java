package biz.binarystar.testing.InterviewSample1;

/**
* This is an ATMOperation interface that defines APIs to be called when an ATM Operation
* takes place. ATMOperation are general level operations that happens on every ATM.
* Debit is recorded when you add money to your account. Credit is recorded when you
* withdraw money from your account. 
* 
* 
* @author  BinaryStar
* @version 1.0
* @since   2016-08-15
*/
public interface ATMOperations {
	
	/**
	 * Operation of debit would be used to add money to your account
	 * TODO: You need to add all necessary parameters to open an account
	 * @return updated balance on account after debit (deposit)
	 */
	public double debit(String accountNumber, double amount);
	
	/**
	 * Operation of credit would be used to withdraw money from account
	 * TODO: You need to add all necessary parameters to open an account
	 * @return updated balance after credit (withdrawal)
	 */
	public double credit(String accountNumber, double amount);
	
	/**
	 * Operation of check balance would be used to see updated balance
	 * TODO: You need to add all necessary parameters to open an account
	 * @return updated balance
	 */
	public double getBalance(String accountNumber);
	
	/**
	 * Operation of open account would be used to open the account
	 * TODO: You need to add all necessary parameters to open an account
	 * @return account number
	 */
	public String openAccount(String accountType, double openingBalance);
	
	/**
	 * Operation of close account would be used to close the account
	 * TODO: You need to add all necessary parameters to close an account
	 * @return true iff account closing successful
	 */
	public boolean closeAccount(String accountNumber);
	
	/**
	 * Operation of check if account if flagged or not
	 * TODO: You need to add all necessary parameters to close an account
	 * @return true iff account is flagged
	 */
	public boolean isFlagged(String accountNumber);

}
